# aws-python-rpc-check-produced-blocks

Simple AWS lambda function to scrape the NEAR RPC endpoint on a 1 minute interval and check if a specific validator has missed any blocks. If so, it will send a SMS message.

## Prerequisites
- Must have the serverless cli installed for deployment  https://www.serverless.com/framework/docs/getting-started/
- Docker (Please use system specific instructions for installing Docker)
- Install the serverless-python-requirements plugin, follow instructions at https://github.com/UnitedIncome/serverless-python-requirements
- Functioning AWS CLI/SDK with active IAM access and permissions https://docs.aws.amazon.com/general/latest/gr/aws-access-keys-best-practices.html
- If using Twilio, you will need a twilio phone number, account SID, and Auth token https://www.twilio.com/docs/sms/quickstart/python#get-a-phone-number

# Deploy function

Once the serverless CLI is installed

Update the serverless.yml file's environment variables to match the desired values (lines 22-29). Change test_sms to "True" if you would like to ensure the SMS text message functionality is working. Return the value to false when you are ready to only alert on missing blocks.

```
validator: "" 
rpc_endpoint: "https://rpc.openshards.io"
rpc_method: "validators"
rpc_jsonrpc: "2.0"
rpc_id: "dontcare"
sms_number: ""
message: "Validator has missed blocks."
test_sms: False
sms_provider: sns # options are sms or twilio
# Twilio options
twilio_account_sid: ""
twilio_auth_token: ""
twilio_phone_number: ""
```

Once ready, run the following commands

```
git clone https://gitlab.com/zachslavin/aws-python-rpc-near-produced-blocks.git
cd aws-python-rpc-check-produced-blocks
sls plugin install -n serverless-python-requirements
serverless deploy
```



