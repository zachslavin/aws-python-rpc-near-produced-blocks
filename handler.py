import json
import boto3
import os
import logging
from datetime import datetime, timezone
from twilio.rest import Client
from jsonrpcclient.clients.http_client import HTTPClient

# Retrieve environment variables
rpc_endpoint = os.environ["rpc_endpoint"]
rpc_jsonrpc = os.environ["rpc_jsonrpc"]
rpc_method = os.environ["rpc_method"]
rpc_id = os.environ["rpc_id"]
validator = os.environ["validator"]
sms_number = os.environ["sms_number"]
message = os.environ["message"]
test_sms = os.environ["test_sms"]
sms_provider = os.environ["sms_provider"]
twilio_account_sid = os.environ["twilio_account_sid"]
twilio_auth_token = os.environ["twilio_auth_token"]
twilio_phone_number = os.environ["twilio_phone_number"]

# Set global variable
lastMissedBlocks = 0

# Create logger
logging.getLogger().setLevel(logging.INFO)

def calculate_missed_blocks(response):
    global lastMissedBlocks
    missed_blocks = None
    for v in response:
        current_validators = json.loads(json.dumps(v))
        if current_validators["account_id"] == validator: 
            if lastMissedBlocks > current_validators["num_expected_blocks"]:
                logging.info("Resetting lastMissedBlocks")
                lastMissedBlocks = 0
            missed_blocks = current_validators["num_expected_blocks"] - current_validators["num_produced_blocks"]
            logging.info("Missed blocks: {}".format(missed_blocks))
    if missed_blocks is None:
        logging.info("Validator not found in current list of validators")  
    return missed_blocks

def send_sms(sms_number, message, missingblocks=0):
    datetime_object = datetime.now(timezone.utc)
    if sms_provider.lower() == "sns":
        client = boto3.client('sns')
        smsattrs = {'AWS.SNS.SMS.SMSType': {'DataType': 'String', 'StringValue': 'Transactional'}}
        message = client.publish(
            PhoneNumber=sms_number,
            Message="{} Current time in UTC: {}. Missing {} blocks".format(message, datetime_object, missingblocks),
            MessageAttributes= smsattrs
            )
        logging.info("Sending SMS")
        logging.info(message)
    elif sms_provider.lower() == "twilio":
        client = Client(twilio_account_sid, twilio_auth_token)
        message = client.messages.create(
            body = "{} Current time in UTC: {}. Missing {} blocks".format(message, datetime_object, missingblocks),
            from_ = twilio_phone_number,
            to = sms_number
        )
        logging.info("Sending SMS")
        logging.info(message.sid)
    else:
        logging.warn("SMS Provider does not match. SMS not sent")
    
def near_rpc(event, context):
    global lastMissedBlocks
    client = HTTPClient(rpc_endpoint)
    payload = {"jsonrpc": "{}".format(rpc_jsonrpc), "method": "{}".format(rpc_method), "id": "{}".format(rpc_id), "params": [None]}
    try:
        response = client.send(json.dumps(payload))
        diff = calculate_missed_blocks(json.loads(response.text)['result']['current_validators'])
        if diff > 0 and diff > lastMissedBlocks:
            send_sms(sms_number, message, missingblocks=diff)
            lastMissedBlocks = diff
        else:
            logging.info(diff)
            if test_sms.lower() == "true":
                send_sms(sms_number, "Test")
    except Exception as e:
        print(e)
        logging.debug(e)

if __name__ == "__main__":
    event = ""
    context = ""
    near_rpc(event, context)
